# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Web XLS contacts',
    'category': 'Tools',
    'version': '1.0',
    'description':
        """
Odoo Web XLS view contacts.
========================

This module provides a Web XLS view for contacts.
        """,
    'depends': ['contacts'],
    'data': [
        'views/contract_view.xml',
    ],
    'qweb': [
    ],
    'bootstrap': True,  
}
