# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Web xls',
    'category': 'Tools',
    'version': '1.0',
    'description':
        """
Odoo Web xls view module.
========================

This module provides an xls view for the Odoo Web Client.
        """,
    'depends': ['web'],
    'data': [
        'views/webclient_templates.xml',
    ],
    'qweb': [
        "static/src/xml/web_xls.xml",
    ],
    'bootstrap': True,  # load translations for login screen
}
